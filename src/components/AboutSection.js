import React from 'react'
import './AboutSection.css'
import { Link } from 'react-router-dom'
function AboutSection() {
    return (
        <>
            <div className='about-container'>
                <h1 className='hdr'>About Me</h1>
                <div className='about-info-container'>
                    <div className='about-text'>
                        My name is Negar Yousefian. I study computer engineering and I am a Front-end Web Developer.
                        I also have some exprience in UI-UX Design.
                        I usually work with React and try to improve my skills.<br/><br/>
                           
                    </div>
                    <div className='about-skills'>
                        <div className='about-skill-item ht' data-label='HTML'></div>
                        <div className='about-skill-item cs' data-label='CSS'></div>
                        <div className='about-skill-item jv' data-label='JavaScript'></div>
                        <div className='about-skill-item rct' data-label='React'></div>
                        <div className='about-skill-item pt' data-label='PhotoShop'></div>
                    </div>

                </div>

            </div>
        </>
    )
}

export default AboutSection
